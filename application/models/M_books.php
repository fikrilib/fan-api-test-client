<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_books extends CI_Model {
	public function __construct(){
		parent::__construct();
	}
	
	public function get_all(){
		$this->db->select('books.*,category.*');
		$this->db->from('t_books books');
		$this->db->join('m_category category', 'books.category_code = category.category_code', 'INNER');
		$this->db->order_by('books.book_id', 'asc');
		return $this->db->get();
	}
	/*Categories*/
	public function get_categories(){
		$this->db->select('*');
		$this->db->from('m_category');
		return $this->db->get();
	}
	
	public function insert($data){
		$this->db->trans_start();
		$this->db->insert('t_books', $data);
		$this->db->trans_complete(); 
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return FALSE;
		} else {
			$this->db->trans_commit();
			return TRUE;
		}
	}
	public function delete($id){
		$this->db->trans_start();
		$this->db->where('book_id', $id);
		$this->db->delete('t_books');
		$this->db->trans_complete(); 
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return FALSE;
		} else {
			$this->db->trans_commit();
			return TRUE;
		}
	}
	
	public function update($data){
		$this->db->trans_start();
		$this->db->where(array('book_id'=>$data['book_id']));
		$this->db->update('t_books', $data);
		$this->db->trans_complete(); 
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return FALSE;
		} else {
			$this->db->trans_commit();
			return TRUE;
		}
	}
	
}