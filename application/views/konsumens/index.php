<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $title; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"/>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<style>
    .submitted .ng-invalid{
        border: 1px solid red;
    }
	</style>
</head>
<body>
<div class="container">
	<h3 class="page-header"><?php echo $title; ?></h3>
	<button class="btn btn-primary pull-right" data-toggle="modal" data-target="#add_konsumen_modal" href="javascript:addNew()">Tambah Konsumen</button>
	<hr>
	<?=$this->session->userdata('message_action') ?>
	<?php $No = 1; ?>
	<table class="table">
		<thead>
		<tr>
			<th>No</th>
			<th>Nama Konsumen</th>
			<th>Nominal</th>
			<th>Kode Transaksi</th>
			<th>Status</th>
			<th>Tanggal Dibuat</th>
			<th>Tangga Diubah</th>
			<th>Aksi</th>
		</tr>
		</thead>
		<tbody>
		<?php foreach($konsumens_tg as $val){if(isset($val->id)){?>
		<tr>
			<td><?php echo $No;?></td>
			<td><?php echo $val->nama;?></td>
			<td><?php echo $val->nominal;?></td>
			<td><?php echo $val->kode_trans;?></td>
			<td><?php echo $val->status;?></td>
			<td><?php echo $val->createdAt;?></td>
			<td><?php echo $val->updatedAt;?></td>
			<td>
				<a href="javascript:update(<?= $No; ?>,<?= $val->id;?>)" id="update<?= $No; ?>" data-nama="<?= $val->nama; ?>" data-nominal="<?= $val->nominal; ?>" data-kode_trans="<?= $val->kode_trans; ?>" data-status="<?= $val->status; ?>" data-toggle="tooltip" data-placement="top" title="Edit <?= $val->nama; ?>" class="btn btn-sm btn-warning btn-flat">Edit</a>
				<a href="<?= base_url()?>index.php/konsumens/delete_konsumen/<?= $val->id; ?>/<?= $val->status; ?>" onclick="return confirm('Yakin akan menghapus data <?= $val->nama; ?>?')" data-toggle="tooltip" data-placement="top" title="Hapus <?= $val->nama; ?>" class="btn btn-sm btn-danger btn-flat">Delete</a>
			</td>
		</tr>
		<?php $No++;}} ?>
		<?php foreach($konsumens_tb as $val){if(isset($val->id)){?>
		<tr>
			<td><?php echo $No;?></td>
			<td><?php echo $val->nama;?></td>
			<td><?php echo $val->nominal;?></td>
			<td><?php echo $val->kode_trans;?></td>
			<td><?php echo $val->status;?></td>
			<td><?php echo $val->createdAt;?></td>
			<td><?php echo $val->updatedAt;?></td>
			<td>
				<a href="javascript:update(<?= $No; ?>,<?= $val->id;?>)" id="update<?= $No; ?>" data-nama="<?= $val->nama; ?>" data-nominal="<?= $val->nominal; ?>" data-kode_trans="<?= $val->kode_trans; ?>" data-status="<?= $val->status; ?>" data-toggle="tooltip" data-placement="top" title="Edit <?= $val->nama; ?>" class="btn btn-sm btn-warning btn-flat">Edit</a>
				<a href="<?= base_url()?>index.php/konsumens/delete_konsumen/<?= $val->id; ?>/<?= $val->status; ?>" onclick="return confirm('Yakin akan menghapus data <?= $val->nama; ?>?')" data-toggle="tooltip" data-placement="top" title="Hapus <?= $val->nama; ?>" class="btn btn-sm btn-danger btn-flat">Delete</a>
			</td>
		</tr>
		<?php $No++;}} ?>
		</tbody>
	</table>

<div id="add_konsumen_modal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document"><div class="modal-content">
		<form class="form-horizontal" id="formkonsumen" action="<?php echo base_url();?>index.php/konsumens/add_konsumen"  method="post">
		<div class="modal-body">
			<div class="form-group">
				<label>Nama Konsumen</label>
					<input type="text" class="form-control" name="nama" placeholder="Nama Konsumen" required/>								
			</div>
			<div class="form-group">
				<label>Nominal</label>
					<input type="number" class="form-control" name="nominal" placeholder="Nominal" required/>								
			</div>
			<div class="form-group">
				<label>Kode Transaksi</label>
					<input type="text" class="form-control rupiah" name="kode_trans" placeholder="Kode Transaksi" required/>								
			</div>
			<div class="form-group">
				<label>Status</label>
					<select class="form-control" name="status" readonly>
						<option value="0" disabled="" selected="">Pilih Status</option>
						<option value="1">Transaksi Berhasil</option>
						<option value="2">Transaksi Gagal</option>
					</select>								
			</div>
		</div>
		<div class="modal-footer">
			<a data-dismiss="modal" class="btn btn-warning btn-flat">Batal</a>
			<button type="submit" id="submit-form" class="btn btn-primary btn-flat">Simpan</button>
		</div>
	</form>
	</div>
	</div>
</div>

<div id="edit_konsumen_modal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document"><div class="modal-content">
		<form class="form-horizontal" id="formkonsumen" action="<?php echo base_url();?>index.php/konsumens/edit_konsumen"  method="post">
		<div class="modal-body">
			<div class="form-group">
				<label>Nama Konsumen</label>
					<input type="text" class="form-control" name="nama" placeholder="Nama Konsumen" required/>								
			</div>
			<div class="form-group">
				<label>Nominal</label>
					<input type="number" class="form-control" name="nominal" placeholder="Nominal" required/>								
			</div>
			<div class="form-group">
				<label>Kode Transaksi</label>
					<input type="text" class="form-control rupiah" name="kode_trans" placeholder="Kode Transaksi" required/>								
			</div>
			<div class="form-group">
				<label>Status</label>
					<select class="form-control" name="status" readonly>
						<option value="0" disabled="" selected="">Pilih Status</option>
						<option value="1">Transaksi Berhasil</option>
						<option value="2">Transaksi Gagal</option>
					</select>								
			</div>
			<input type="hidden" name="id" id="id" />
		</div>
		<div class="modal-footer">
			<a data-dismiss="modal" class="btn btn-warning btn-flat">Batal</a>
			<button type="submit" id="submit-form" class="btn btn-primary btn-flat">Simpan</button>
		</div>
	</form>
	</div>
	</div>
</div>
<script type="text/javascript">
	function addNew(){
		$("input[name='nama']").val("");
		$("input[name='nominal']").val("");
		$("input[name='kode_trans']").val("");
		$("select[name='status']").val(0);
		$("#id").val(0);
		$("#submit-form").text("Simpan");
		$("#add_konsumen_modal").modal("show");
	}
	function update(no,id){
		$("input[name='nama']").val($("#update"+no).data("nama"));
		$("input[name='nominal']").val($("#update"+no).data("nominal"));
		$("input[name='kode_trans']").val($("#update"+no).data("kode_trans"));
		$("select[name='status']").val($("#update"+no).data("status"));
		$("#myModalLabel").text("Edit Data Konsumen");
		$("#id").val(id);
		$("#submit-form").text("Edit");
		$("#edit_konsumen_modal").modal("show");
	}
</script>

</div>
</body>
</html>