<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/vendor/autoload.php';
use \Firebase\JWT\JWT;
	
class Konsumens extends CI_Controller {
	public function __construct(){
		parent::__construct();
	}
	
	public function index(){
		$this->data['konsumens_tg'] = JWT::decode(str_replace(array('"'), "", file_get_contents('http://localhost:8081/api/konsumens_tg')),'fan-api-test',array('HS256'));
		$this->data['konsumens_tb'] = JWT::decode(str_replace(array('"'), "", file_get_contents('http://localhost:8081/api/konsumens_tb')),'fan-api-test',array('HS256'));
		$this->data['title']='Daftar Konsumen';
		$this->load->view('konsumens/index',$this->data);
	}

	public function add_konsumen(){
		if($this->input->post('status') == 1){
			$host = 'http://localhost:8081/api/konsumens_tb';
		} else {
			$host = 'http://localhost:8081/api/konsumens_tg';
		}
		
		$token = array (
		  'nama' => $this->input->post('nama'),
		  'nominal' => $this->input->post('nominal'),
		  'kode_trans' => $this->input->post('kode_trans')
		);

		$json = '{"datajson":"'.JWT::encode($token,'fan-api-test','HS512').'"}';
		
		$ch = curl_init($host);
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: '.strlen($json))
		);

		$response = curl_exec($ch);
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		$body = substr($response, $header_size);
		$return = json_decode($body,true);
		$close = curl_close($ch);
		if ( $httpCode != 200 ){
			$this->session->set_flashdata('message_action', '<div class="alert alert-warning" role="alert">  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$return['msg'][0]['message'].'</div>');
			redirect('konsumens/index','refresh');
		} else {
			$this->session->set_flashdata('message_action', '<div class="alert alert-success" role="alert">  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$return['msg'].'</div>');
			redirect('konsumens/index','refresh');
		}
		
	}
	
	public function edit_konsumen(){
		if($this->input->post('status') == 1){
			$host = 'http://localhost:8081/api/konsumens_tb';
		} else {
			$host = 'http://localhost:8081/api/konsumens_tg';
		}
		
		$token = array (
		  'id' => $this->input->post('id'),
		  'nama' => $this->input->post('nama'),
		  'nominal' => $this->input->post('nominal'),
		  'kode_trans' => $this->input->post('kode_trans')
		);

		$json = '{"datajson":"'.JWT::encode($token,'fan-api-test','HS512').'"}';
		
		$ch = curl_init($host);
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: '.strlen($json))
		);

		$response = curl_exec($ch);
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		$body = substr($response, $header_size);
		$return = json_decode($body,true);
		$close = curl_close($ch);
		if ( $httpCode != 200 ){
			$this->session->set_flashdata('message_action', '<div class="alert alert-warning" role="alert">  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$return['msg'][0]['message'].'</div>');
			redirect('konsumens/index','refresh');
		} else {
			$this->session->set_flashdata('message_action', '<div class="alert alert-success" role="alert">  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$return['msg'].'</div>');
			redirect('konsumens/index','refresh');
		}
		
	}
	
	public function delete_konsumen($id, $status){
		if($status == 1){
			$host = 'http://localhost:8081/api/konsumens_tb';
		} else {
			$host = 'http://localhost:8081/api/konsumens_tg';
		}
		
		$token = array ('id' => $id);

		$json = '{"datajson":"'.JWT::encode($token,'fan-api-test','HS512').'"}';
		
		$ch = curl_init($host);
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: '.strlen($json))
		);

		$response = curl_exec($ch);
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		$body = substr($response, $header_size);
		$return = json_decode($body,true);
		$close = curl_close($ch);

		if ( $httpCode != 200 ){
			print_r($return);
			$this->session->set_flashdata('message_action', '<div class="alert alert-warning" role="alert">  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$httpCode.' '.$return['msg'][0]['message'].'</div>');
			#redirect('konsumens/index','refresh');
		} else {
			$this->session->set_flashdata('message_action', '<div class="alert alert-success" role="alert">  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$return['msg'].'</div>');
			redirect('konsumens/index','refresh');
		}
	}
	
}