/*
 Navicat Premium Data Transfer

 Source Server         : localhost8077
 Source Server Type    : MySQL
 Source Server Version : 100136
 Source Host           : localhost:3310
 Source Schema         : fan-api-test

 Target Server Type    : MySQL
 Target Server Version : 100136
 File Encoding         : 65001

 Date: 23/10/2018 17:14:01
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_tb_konsumens
-- ----------------------------
DROP TABLE IF EXISTS `t_tb_konsumens`;
CREATE TABLE `t_tb_konsumens`  (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nominal` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kode_trans` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT 1,
  `createdAt` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `updatedAt` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `tb_id`(`id`) USING BTREE,
  UNIQUE INDEX `kode_trans`(`kode_trans`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 76 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of t_tb_konsumens
-- ----------------------------
INSERT INTO `t_tb_konsumens` VALUES (75, 'TB-Nama', '10000', 'TB-1', 1, '2018-10-23 09:31:44', '2018-10-23 09:31:44');

-- ----------------------------
-- Table structure for t_tg_konsumens
-- ----------------------------
DROP TABLE IF EXISTS `t_tg_konsumens`;
CREATE TABLE `t_tg_konsumens`  (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nominal` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kode_trans` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT 2,
  `createdAt` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `updatedAt` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `tg_id`(`id`) USING BTREE,
  UNIQUE INDEX `tg_kode_trans`(`kode_trans`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of t_tg_konsumens
-- ----------------------------
INSERT INTO `t_tg_konsumens` VALUES (3, 'TG-Nama-Edit', '30000', 'TG-1', 2, '2018-10-23 09:32:05', '2018-10-23 09:40:44');

SET FOREIGN_KEY_CHECKS = 1;
